import {Component, OnInit} from '@angular/core';

import {HistoryService} from './services/history.service';
import {SharedService} from './services/shared.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  title = 'app works!';
  codeConfig = {};
  code = '';
  subscription = null;
  // ...
  onButtonClick() {
    // this.title = 'Hello from Kendo UI!';
  }
  // ...
  onCodeChange() {
    this.sharedService.changeCode(this.code);
  }
  constructor(private historyService: HistoryService, private sharedService: SharedService) {
    // Configuration code mirror
    this.codeConfig = {
      lineNumbers: true, // show line numbers
      lineWrapping: true, // show line wrapping
      foldGutter: true,
      extraKeys: {'Ctrl-Q': function(cm){ cm.foldCode(cm.getCursor()); }},
      gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
      mode: 'sparql', // current mode
      autoCloseBrackets: true,
      matchBrackets: true // Highlighting brackets
    };
    // init shared service
    this.sharedService = sharedService;
    // update codeData - var from history service
    this.historyService.codeData = `PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?name ?email
WHERE {
 ?person a foaf:Person.
 ?person foaf:name ?name.
 ?person foaf:mbox ?email.
}`;
  }
  // ngOnInit() {
  //   this.subscription = this.sharedService.getEmittedValue()
  //       .subscribe(item => this.code = item);
  // }
}
