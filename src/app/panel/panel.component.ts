import { Component, OnInit } from '@angular/core';
import {PanelBarItemModel} from '@progress/kendo-angular-layout';
import {HistoryService} from '../services/history.service';
import {SharedService} from '../services/shared.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  styles: [`
      .custom-template {
          padding: 30px;
          text-align: center;
      }
  `]
})
export class PanelComponent implements OnInit {
    private toolbox = [
        {title: 'Beatufy code', action: this.historyService.beautifyCode, active: true },
        {title: 'Remove MINUS', action: this.historyService.removeMinus, active: true },
        {title: 'Expand/Compact URI', action: this.historyService.expandCompactUri, active: true },
        {title: 'Remove Singleton', action: this.historyService.removeSingleton, active: true },
        {title: 'Add SingletonProperty', action: this.historyService.addSingleton, active: true },
    ];
    public items: ItemHistoryList[] = [];

    constructor(public historyService: HistoryService, private sharedService: SharedService) {
        this.sharedService.getEmittedValueHistory()
            .subscribe(item =>  { this.onHistoryChange(item); });
    }
    ngOnInit() {}
    // ...
    public onPanelChange(event: any) { console.log('stateChange: ', event); }
    // ...
    public onHistoryChange(item: any) {
        const itemHistory = new ItemHistoryList(item);
        if ( itemHistory === null ) {
            return false;
        }
        this.items.push(item);
    }
    // ...
    public beautifyCode(item) {
        return this.historyService.beautifyCode();
    }

}
class ButtonItem {
    public title;
    public action: any;
    public active;
    // ...
    constructor(data) {
        this.title = data.title || 'Default title';
        this.action = data.action || null;
        this.active = data.active || false;
    }
}
class ItemHistoryList {
    public title;
    public index;
    constructor(data) {
        const nl = null;
        if ( typeof data === 'undefined' ) {
            return nl;
        }
        if ( typeof data.index === 'undefined' ) {
            return nl;
        }
        if ( typeof  data.title === 'undefined' ) {
           const tD = new Date;
           const strDate = this.formatDate(tD);
           if ( strDate === null ) {
               return nl;
           }
           data.title = 'Operation ' + strDate;
        }
        this.title = data.title;
        this.index = data.index;

        return this;
    }
    // ...
    private formatDate(date: Date) {
        if ( typeof  date === 'undefined') {
            return null;
        }
        let strDate = '';
        strDate = `${date.getDay()}.${date.getMonth()}.${date.getFullYear()} 
            ${date.getHours()}:${date.getSeconds()}:${date.getMilliseconds()}`;
        return strDate;
    }
}
