import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

// KendoUI
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { PanelComponent } from './panel/panel.component';
import { HeadPanelComponent } from './head-panel/head-panel.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { Codemirror } from 'codemirror';

import 'codemirror/addon/display/placeholder';

import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/addon/edit/closebrackets';
import 'codemirror/addon/edit/trailingspace';
import 'codemirror/addon/edit/trailingspace';

import 'codemirror/addon/fold/foldcode';
import 'codemirror/addon/fold/foldgutter';
import 'codemirror/addon/fold/brace-fold';
import 'codemirror/addon/fold/comment-fold';
import 'codemirror/addon/fold/indent-fold';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/sparql/sparql';

import 'codemirror/addon/hint/show-hint.js';
import 'codemirror/addon/search/searchcursor.js';
import 'codemirror/addon/edit/matchbrackets.js';
import 'codemirror/addon/runmode/runmode.js';

import { CodemirrorModule } from 'ng2-codemirror';
import { SortableModule } from '@progress/kendo-angular-sortable';

import { LocalStorageModule } from 'angular-2-local-storage';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HistoryService } from './services/history.service';
import {SharedService} from './services/shared.service';


@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    HeadPanelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    // ...
    LocalStorageModule.withConfig({
      prefix: 'sparql-editor',
      storageType: 'localStorage'
    }),
    // ...
    ButtonsModule,
    LayoutModule,
    CodemirrorModule,
    SortableModule,
    // ...

  ],
  providers: [SharedService, HistoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
