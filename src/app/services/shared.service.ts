import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable()
export class SharedService {
  @Output() fire: EventEmitter<any> = new EventEmitter();
  @Output() code: EventEmitter<any> = new EventEmitter();
  @Output() history: EventEmitter<any> = new EventEmitter();
  @Output() dataChangeObserver: EventEmitter<any> = new EventEmitter();

  data = null;
  constructor() {
    console.log('shared service started');
  }

  changeCode(code) {
    console.log('change started');
    this.code.emit(code);
  }
  // ...
  changeHistory(historyItem) {
    this.history.emit(historyItem);
  }

  getEmittedValueCode() {
    console.log('Emit data');
    return this.code;
  }

  getEmittedValueHistory() {
    console.log('Emit history');
    console.log(this.history);
    return this.history;
  }

  setData(data: any) {
    this.data = data;
    this.dataChangeObserver.emit(this.data);
    return this.dataChangeObserver;
  }

}
