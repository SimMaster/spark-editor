import {Injectable, Input, OnChanges, OnInit} from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {SharedService} from './shared.service';

const  limitHistory = 100;
@Injectable()
export class HistoryService {
  private objectSmallHistory = {
    past: [],
    present: null,
    future: []
  };
  public codeData = null;
  public historyItems = [];

  subscription = null;
  // ...
  static makeArrayFromString(string) {
    if (typeof string === 'undefined') { return null; }
    if (string === '' || string === null ) { return null; }
    return string.split('\n');
  }
  // ...
  static makeStringFromArray(arrLines) {
    if (typeof arrLines === 'undefined') { return null; }
    if (Object.prototype.toString.call(arrLines) !== '[object Array]') { return null; }
    return arrLines.join('\n');
  }
  constructor(private localStorageService: LocalStorageService, private sharedService: SharedService) {
    this.sharedService.getEmittedValueCode()
        .subscribe(item =>  { this.onUpdateCode(item); });
  }
  public onItemHistoryClick(item) {
    console.log(item);
    console.log(this.codeData);
  }
  // ...
  private onUpdateCode(item) {
    this.insertHistory(item);
    this.codeData = item;
    console.log(this.codeData);
    // Add History
    // this.sharedService.changeHistory({ index: this.getCountPastData()});
  }
  // ...
  private insertHistory(item) {
    const codeForStorage = item;
    if ( this.objectSmallHistory.present !== null ) {
      const dataCode = this.objectSmallHistory.present;
      this.objectSmallHistory.past.push(dataCode);
    }
    this.objectSmallHistory.present = codeForStorage;
  }
  // ...
  private getCountPastData()  {
    return this.objectSmallHistory.past.length - 1;
  }


  // TODO: Move methods to service
  // ...
  public beautifyCode() {
    if (this.codeData !== '' && this.codeData !== null) {
      let prefix = '';
      const arrSplitNewLine = HistoryService.makeArrayFromString(this.codeData);
      if (arrSplitNewLine === null ) {
        console.log('makeArrayFromString not work');
        return false;
      }
      const arrUpdatedString = [];
      for (let line of arrSplitNewLine) {
        line = line.trim();
        if (line === '') {
          continue;
        }
        if (line.indexOf('{') >= 0) {
          line = prefix + line;
          arrUpdatedString.push(line);
          prefix += '  ';
          continue;
        }else if (line.indexOf('}') >= 0) {
          if (prefix.length <= 2) {
            prefix = '';
          } else {
            prefix = prefix.slice(0, -2);
          }
        }
        line = prefix + line;
        arrUpdatedString.push(line);
      }
      const updatedString = HistoryService.makeStringFromArray(arrUpdatedString);
      if (updatedString === null ) {
        console.log('MakeArrayFrom String not work');
        return false;
      }
      this.codeData = updatedString;
    }
  }
  // ...
  removeMinus() {
    if (this.codeData !== '' && this.codeData !== null) {
      const arrSplitNewLine = HistoryService.makeArrayFromString(this.codeData);
      if (arrSplitNewLine === null ) {
        console.log('makeArrayFromString not work');
        return false;
      }
      const arrUpdatedString = [];
      let blockMinus = false;
      for (const line of arrSplitNewLine) {
        if (line.indexOf('MINUS') >= 0) {
          blockMinus = true;
        }
        if (line.indexOf('}') >= 0) {
          if (blockMinus === true) {
            blockMinus = false;
            continue;
          }
        }
        if (!blockMinus) {
          arrUpdatedString.push(line);
        }
      }
      // ...
      const updatedString = HistoryService.makeStringFromArray(arrUpdatedString);
      if (updatedString === null ) {
        console.log('MakeArrayFrom String not work');
        return false;
      }
      this.codeData = updatedString;
      // Update code
      this.beautifyCode();
    }
  }
  // ...
  expandCompactUri() {}
  // ...
  removeSingleton() {}
  // ...
  addSingleton() {}




}
