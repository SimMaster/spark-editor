import { SparkEditorPage } from './app.po';

describe('spark-editor App', () => {
  let page: SparkEditorPage;

  beforeEach(() => {
    page = new SparkEditorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
