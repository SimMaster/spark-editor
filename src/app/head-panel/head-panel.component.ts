import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../services/history.service';
import {SharedService} from '../services/shared.service';

@Component({
  selector: 'app-head-panel',
  templateUrl: './head-panel.component.html',
  styleUrls: ['./head-panel.component.scss']
})
export class HeadPanelComponent implements OnInit {
  title = 'SRQL-light';
  subTitle = 'Enter your Sparql code:';
  constructor(private historyService: HistoryService, private sharedService: SharedService) { }

  ngOnInit() {
  }

}
